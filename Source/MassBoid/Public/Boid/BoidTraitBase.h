// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MassEntityTraitBase.h"
#include "MassEntityTypes.h"
#include "BoidTraitBase.generated.h"

USTRUCT()
struct FBoidConfigFragment : public FMassSharedFragment
{

    GENERATED_BODY()

    UPROPERTY(EditAnywhere)
    float SensorRange;
    UPROPERTY(EditAnywhere)
    float SensorAngle;
    UPROPERTY(EditAnywhere)
    float CohesionWeight;
    UPROPERTY(EditAnywhere)
    float AlignmentWeight;
    UPROPERTY(EditAnywhere)
    float SeparationWeight;
};
/**
 *
 */
UCLASS() class MASSBOID_API UBoidTraitBase : public UMassEntityTraitBase
{
    GENERATED_BODY()

  protected:
    virtual void BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, UWorld& World) const override;

    UPROPERTY(EditAnywhere)
    FBoidConfigFragment BoidConfig;
};

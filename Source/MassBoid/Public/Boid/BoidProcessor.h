// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MassProcessor.h"
#include "BoidProcessor.generated.h"

/**
 *
 */
UCLASS()
class MASSBOID_API UBoidProcessor : public UMassProcessor
{
	GENERATED_BODY()

public:
	UBoidProcessor();

protected:
	virtual void ConfigureQueries() override;
	virtual void Execute(UMassEntitySubsystem &EntitySubSystem, FMassExecutionContext &Context) override;

private:
	FMassEntityQuery EntityQuery;
};

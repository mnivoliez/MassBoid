// Fill out your copyright notice in the Description page of Project Settings.

#include "Boid/BoidTraitBase.h"
#include "MassEntityTemplateRegistry.h"

void UBoidTraitBase::BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, UWorld& World) const
{
    UMassEntitySubsystem* EntitySubsystem = UWorld::GetSubsystem<UMassEntitySubsystem>(&World);
    check(EntitySubsystem);

    const FConstSharedStruct Config = EntitySubsystem->GetOrCreateConstSharedFragment(UE::StructUtils::GetStructCrc32(FConstStructView::Make(BoidConfig)), BoidConfig);
    BuildContext.AddConstSharedFragment(Config);
}

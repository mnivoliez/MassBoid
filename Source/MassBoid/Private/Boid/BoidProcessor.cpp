// Fill out your copyright notice in the Description page of Project Settings.

#include "Boid/BoidProcessor.h"
#include "Boid/BoidTraitBase.h"
#include "MassEntityTemplateRegistry.h"
#include "MassCommonFragments.h"
#include "MassMovementFragments.h"

UBoidProcessor::UBoidProcessor()
{
    bAutoRegisterWithProcessingPhases = true;
    ExecutionFlags = (int32)EProcessorExecutionFlags::All;
    ExecutionOrder.ExecuteBefore.Add(UE::Mass::ProcessorGroupNames::Avoidance);
}

void UBoidProcessor::ConfigureQueries()
{
    EntityQuery.AddRequirement<FTransformFragment>(EMassFragmentAccess::ReadWrite);
    EntityQuery.AddRequirement<FMassVelocityFragment>(EMassFragmentAccess::ReadWrite);
    EntityQuery.AddConstSharedRequirement<FBoidConfigFragment>(EMassFragmentPresence::All);
}

void UBoidProcessor::Execute(UMassEntitySubsystem& EntitySubSystem, FMassExecutionContext& Context)
{
    EntityQuery.ForEachEntityChunk(EntitySubSystem, Context, [this](FMassExecutionContext& Context) {
        const TArrayView<FTransformFragment> TransformList = Context.GetMutableFragmentView<FTransformFragment>();
        const TArrayView<FMassVelocityFragment> VelocityList = Context.GetMutableFragmentView<FMassVelocityFragment>();

        const FBoidConfigFragment& BoidConfig = Context.GetConstSharedFragment<FBoidConfigFragment>();

        const float WorldDeltaTime = Context.GetDeltaTimeSeconds();

        for (int EntityIndex = 0; EntityIndex < Context.GetNumEntities(); ++EntityIndex)
        {
            FTransform& CurrentBoidTransform = TransformList[EntityIndex].GetMutableTransform();
            FVector& CurrentBoidVelocity = VelocityList[EntityIndex].Value;

            FVector CurrentBoidForward = CurrentBoidTransform.GetUnitAxis(EAxis::X);

            FVector AverageDeltaPos = FVector::Zero();
            FVector AverageDeltaSpeed = FVector::Zero();
            FVector Separation = FVector::Zero();

            int NumberOfNeighbors = 0;

            for (int OtherEntityIndex = 0; OtherEntityIndex < Context.GetNumEntities(); ++OtherEntityIndex)
            {
                if (OtherEntityIndex == EntityIndex)
                {
                    continue;
                }

                FTransform& OtherBoidTransform = TransformList[OtherEntityIndex].GetMutableTransform();
                FVector& OtherBoidVelocity = VelocityList[OtherEntityIndex].Value;

                FVector OtherBoidForward = OtherBoidTransform.GetUnitAxis(EAxis::X).GetSafeNormal();

                FVector OtherToCurrent = CurrentBoidTransform.GetLocation() - OtherBoidTransform.GetLocation();
                float DistSQ = OtherToCurrent.SquaredLength();
                if (DistSQ < FMath::Square(BoidConfig.SensorRange))
                {
                    FVector NormCurrentToOther = (-OtherToCurrent).GetSafeNormal();
                    float Angle = FMath::Acos(CurrentBoidForward.Dot(NormCurrentToOther));

                    if (FMath::Abs(Angle) < FMath::DegreesToRadians(BoidConfig.SensorAngle))
                    {
                        AverageDeltaPos += OtherBoidTransform.GetLocation();
                        AverageDeltaSpeed += OtherBoidVelocity;
                        Separation += OtherToCurrent / DistSQ;
                    }
                }
            }
            AverageDeltaPos /= NumberOfNeighbors;
            AverageDeltaSpeed /= NumberOfNeighbors;

            FVector CohesionForce = (AverageDeltaPos - CurrentBoidTransform.GetLocation()) * BoidConfig.CohesionWeight * WorldDeltaTime;
            FVector AlignmentForce = (AverageDeltaSpeed - CurrentBoidVelocity) * BoidConfig.AlignmentWeight * WorldDeltaTime;
            FVector SeparationForce = Separation * BoidConfig.SeparationWeight * WorldDeltaTime;

            CurrentBoidVelocity = CurrentBoidVelocity + CohesionForce + AlignmentForce + SeparationForce;
            
            CurrentBoidTransform.SetLocation(CurrentBoidTransform.GetLocation() + CurrentBoidVelocity * WorldDeltaTime);
        }
    });
};
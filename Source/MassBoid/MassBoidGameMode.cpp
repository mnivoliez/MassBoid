// Copyright Epic Games, Inc. All Rights Reserved.

#include "MassBoidGameMode.h"
#include "MassBoidCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMassBoidGameMode::AMassBoidGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}

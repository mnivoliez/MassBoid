// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MassBoid : ModuleRules
{
    public MassBoid(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(
            new string[]
            {
                "MassBoid"
            });

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
        PrivateDependencyModuleNames.AddRange(new string[] { "MassCommon", "MassEntity", "MassMovement", "StructUtils" });
    }
}
